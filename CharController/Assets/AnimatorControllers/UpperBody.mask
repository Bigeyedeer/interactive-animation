%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: UpperBody
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Dirkie_mesh
    m_Weight: 0
  - m_Path: Dirkie_rig
    m_Weight: 0
  - m_Path: Dirkie_rig/root
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-neck
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-neck/ORG-head
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-f_index_01_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-f_index_01_L/ORG-f_index_02_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-f_index_01_L/ORG-f_index_02_L/ORG-f_index_03_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-thumb_01_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-thumb_01_L/ORG-thumb_02_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_L/ORG-upper_arm_L/ORG-forearm_L/ORG-hand_L/ORG-thumb_01_L/ORG-thumb_02_L/ORG-thumb_03_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-f_index_01_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-f_index_01_R/ORG-f_index_02_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-f_index_01_R/ORG-f_index_02_R/ORG-f_index_03_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-thumb_01_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-thumb_01_R/ORG-thumb_02_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-spine/ORG-chest/ORG-shoulder_R/ORG-upper_arm_R/ORG-forearm_R/ORG-hand_R/ORG-thumb_01_R/ORG-thumb_02_R/ORG-thumb_03_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-thigh_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-thigh_L/ORG-shin_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-thigh_L/ORG-shin_L/ORG-foot_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-thigh_L/ORG-shin_L/ORG-foot_L/ORG-toe_L
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-thigh_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-thigh_R/ORG-shin_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-thigh_R/ORG-shin_R/ORG-foot_R
    m_Weight: 0
  - m_Path: Dirkie_rig/root/ORG-hips/ORG-thigh_R/ORG-shin_R/ORG-foot_R/ORG-toe_R
    m_Weight: 0
  - m_Path: Eyes
    m_Weight: 0
